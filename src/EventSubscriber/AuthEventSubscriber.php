<?php

namespace Drupal\wework_connect\EventSubscriber;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthEventSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The OpenID client plugin manager.
   *
   * @var \Drupal\openid_connect\Plugin\OpenIDConnectClientManager
   */
  protected $pluginManager;

  /**
   * @var \Drupal\wework_connect\Plugin\OpenIDConnectClient\OpenIDConnectWeworkClient
   */
  protected $client;

  /**
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    OpenIDConnectClientManager $plugin_manager) {
    $this->configFactory = $config_factory;
    $this->pluginManager = $plugin_manager;

    $clientId = 'wework';
    $configuration = $this->configFactory->get('openid_connect.settings.' . $clientId)
      ->get('settings');
    $this->client = $this->pluginManager->createInstance(
      $clientId,
      $configuration ?: []
    );
  }

  public function onKernelRequestAuthenticate(RequestEvent $event) {
    if (Drupal::currentUser()->id() > 0) {
      return;
    }
    if (empty($this->client) || !$event->isMasterRequest()) {
      return;
    }
    if ($this->client->shouldAuthorize($event->getRequest())) {
      $_SESSION['openid_connect_op'] = 'login';
      $_SESSION['openid_connect_destination'] = $event->getRequest()
        ->getRequestUri();
      $_SESSION['openid_connect_destination'] .= (stripos($_SESSION['openid_connect_destination'], '?')?'&':'?').'from_wework_connect=1';
      $response = $this->client->authorize();
      /**
       * @var $pageCacheKiller \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
       */
      $pageCacheKiller = Drupal::service('page_cache_kill_switch');
      $pageCacheKiller->trigger();
      $event->setResponse($response);
    }
  }

  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestAuthenticate', 1];
    return $events;
  }

}
