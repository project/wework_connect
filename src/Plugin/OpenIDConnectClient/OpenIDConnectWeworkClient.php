<?php


namespace Drupal\wework_connect\Plugin\OpenIDConnectClient;


use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Wework OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for WeWork.
 *
 * @OpenIDConnectClient(
 *   id = "wework",
 *   label = @Translation("WeWork")
 * )
 */
class OpenIDConnectWeworkClient extends OpenIDConnectClientBase
{

  use LoggerChannelTrait;

  protected function logger(): LoggerInterface
  {
    return $this->getLogger('openid_connect_' . $this->pluginId);
  }

  public function getEndpoints(): array
  {
    return [
      'authorization' => 'https://open.weixin.qq.com/connect/oauth2/authorize',
      // 全局的access_token
      // 非oAuth2.0的access_token
      'token' => 'https://qyapi.weixin.qq.com/cgi-bin/gettoken',
      'userinfo' => 'https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo',
      'userDetail' => 'https://qyapi.weixin.qq.com/cgi-bin/user/get',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration()
  {
    return [
      'corpid' => '',
      'secret' => '',
      'agentid' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
  {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Wework has corp id respect to the corporation and agent id respect to one application of the corporation.
    // The "secret" is to pair with the  agent id, not the corp id
    unset($form['client_id'], $form['client_secret']);
    $form['corpid'] = [
      '#title' => $this->t('Corporation ID'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['corpid'],
      '#description' => $this->t('Wework corpid: @link', ['@link' => 'https://open.work.weixin.qq.com/api/doc/90000/90135/90665#corpid'])
    ];
    $form['secret'] = [
      '#title' => $this->t('Secret'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['secret'],
      '#description' => $this->t('Wework secret: @link', ['@link' => 'https://open.work.weixin.qq.com/api/doc/90000/90135/90665#secret'])
    ];
    $form['agentid'] = [
      '#title' => $this->t('Wework Application Id'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['agentid'],
      '#description' => $this->t('Wework agentid: @link', ['@link' => 'https://open.work.weixin.qq.com/api/doc/90000/90135/90665#agentid']),
    ];
    $form['fill_email_method'] = [
      '#title' => $this->t('Fill email method'),
      '#type' => 'select',
      '#options' => [
        'use_email' => $this->t('Use email'),
        'use_mobile' => $this->t('Use mobile'),
        'use_userid' => $this->t('Use userid'),
      ],
      '#default_value' => $this->configuration['fill_email_method'],
      '#description' => $this->t('How to deal with email field? OpenIdConnect need the email field to check whether the user exists in system, but wework user may have no email field or mobile field. If use mobile, the email field equals to MOBILE_NUMBER@wework.com'),
    ];
    $form['should_authorize'] = [
      '#title' => $this->t('Should Authorize Urls'),
      '#description' => $this->t('When user browse these urls, if not login, force redirect to wework.One url per line. Use regex.'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['should_authorize'],
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
  {
// Provider label as array for StringTranslationTrait::t() argument.
    $provider = [
      '@provider' => $this->getPluginDefinition()['label'],
    ];

    // Get plugin setting values.
    $configuration = $form_state->getValues();


    if (empty($configuration['corpid'])) {
      $form_state->setErrorByName('corpid', $this->t('The corpid is missing for @provider.', $provider));
    }

    if (empty($configuration['secret'])) {
      $form_state->setErrorByName('secret', $this->t('The secret is missing for @provider.', $provider));
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function getUrlOptions($scope, GeneratedUrl $redirect_uri): array
  {
    // see https://open.work.weixin.qq.com/api/doc/90000/90135/91022
    return [
      'query' => [
        //企业的CorpID
        'appid' => $this->configuration['corpid'],
        //授权后重定向的回调链接地址，请使用urlencode对链接进行处理
        'redirect_uri' => $redirect_uri->getGeneratedUrl(),
        //返回类型，此时固定为：code
        'response_type' => 'code',
        //应用授权作用域。企业自建应用固定填写：snsapi_base
        'scope' => 'snsapi_base',
        //定向后会带上state参数，企业可以填写a-zA-Z0-9的参数值，长度不可超过128个字节
        'state' => $this->stateToken->create(),
      ],
      'fragment' => 'wechat_redirect',//终端使用此参数判断是否需要带上身份信息
    ];
  }

  public function retrieveTokens($authorization_code): array
  {
    return [
      'id_token' => NULL,
      'access_token' => $authorization_code,
    ];
  }

  public function decodeIdToken($id_token): array
  {
    return [];
  }

  /**
   * {@inheritDoc}
   * @see wework_connect_openid_connect_claims_alter()
   */
  public function retrieveUserInfo($access_token)
  {
    try {
      $code = $access_token;
      $access_token = $this->getWeworkAccessToken();
      $client = $this->httpClient;
      $response = $client->get($this->getEndpoints()['userinfo'], [
        'query' => [
          'access_token' => $access_token,
          'code' => $code,
        ],
      ]);
      /**
       * {
       * "errcode": 0,
       * "errmsg": "ok",
       * "UserId":"USERID",
       * "DeviceId":"DEVICEID"
       * }
       */
      $responseData = (string)$response->getBody();
      /** @noinspection PhpComposerExtensionStubsInspection */
      $userInfo = json_decode($responseData, TRUE);
      $this->logger()
        ->info('Get userinfo from wework server: @info', ['@info' => $responseData]);
      if (!empty($userInfo['OpenId'])) {
        return [
          'sub' => 'OpenId:' . $userInfo['OpenId'],
          'email' => $userInfo['OpenId'] . '@wework.openid.com',
          'DeviceId' => $userInfo['DeviceId'],
          'OpenId' => $userInfo['OpenId'],
          'external_userid' => $userInfo['external_userid']
        ];
      }
      if (empty($userInfo['UserId'])) {
        throw new Exception('Need UserId:' . $responseData);
      }
      $response = $client->get($this->getEndpoints()['userDetail'], [
        'query' => array_filter([
          'access_token' => $access_token,
          'userid' => $userInfo['UserId'],
        ]),
      ]);

      $responseData = (string)$response->getBody();
      /** @noinspection PhpComposerExtensionStubsInspection */
      $userData = json_decode($responseData, TRUE);
      $this->logger()
        ->info('Get user detail from wework server: @info', ['@info' => $responseData]);
      $userData['DeviceId'] = $userInfo['DeviceId'];
      // $userInfo['UserId'] == $userData['userid'], wework name same variable with two styles.
      // sub is primary key in authMap
      // the UserId field is always unique in one corporation
      $userData['sub'] = 'UserId:' . $userInfo['UserId'];
      // back up email field
      // openid connect map email field with the mail field of drupal user.
      $userData['real_email'] = $userData['email'];
      switch ($this->configuration['fill_email_method']) {
        case 'use_mobile':
          $userData['email'] = $userData['mobile'] . '@wework.mobile.com';
          break;
        case 'use_userid':
          $userData['email'] = $userData['userid'] . '@wework.userid.com';
          break;
      }
      if (empty($userData['email'])) {
        throw new Exception('need email:' . $responseData);
      }
      return $userData;
    } catch (Exception $e) {
      $variables = [
        '@message' => 'Could not retrieve user profile information',
        '@error_message' => $e->getMessage(),
      ];
      $this->logger()->error('@message. Details: @error_message', $variables);
      return FALSE;
    }
  }

  /**
   * @throws \Exception
   */
  protected function getWeworkAccessToken(): string
  {
    try {
      $cache = Drupal::cache();
      $cid = __CLASS__ . __FUNCTION__;
      $data = $cache->get($cid);
      if (!$data) {
        $response = $this->httpClient->get($this->getEndpoints()['token'], [
          'query' => [
            'corpid' => $this->configuration['corpid'],
            'corpsecret' => $this->configuration['secret'],
          ],
        ]);
        $responseData = (string)$response->getBody();
        /** @noinspection PhpComposerExtensionStubsInspection */
        $tokenData = json_decode($responseData, TRUE);
        if (!empty($tokenData['errcode']) || empty($tokenData['access_token'])) {
          throw new Exception('Request access_token error:' . $tokenData['errcode'] . ':' . $tokenData['errmsg'] . ':' . $responseData);
        }
        $token = $tokenData['access_token'];
        $cache->set($cid, $token, $tokenData['expires_in'] - 200);
        $this->logger()
          ->info('Get access token from wework server:@access_token', ['@access_token' => $token]);
      } else {
        $token = $data->data;
      }
      return $token;
    } catch (Exception $e) {
      $variables = [
        '@message' => 'Could not get access token',
        '@error_message' => $e->getMessage(),
      ];
      $this->logger()->error('@message. Details: @error_message', $variables);
      return FALSE;
    }
  }

  /**
   * Check whether redirect current request to wework login logic
   * @param Request $request
   * @return bool
   */
  public function shouldAuthorize(Request $request): bool
  {
    if (empty($this->configuration['should_authorize'])) {
      return FALSE;
    }
    if ($request->query->get('from_wework_connect')) {
      return FALSE;
    }
    foreach (explode("\n", $this->configuration['should_authorize']) as $url) {
      $url = trim($url);
      if (empty($url)) {
        continue;
      }
      if (preg_match($url, $request->getRequestUri())) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
